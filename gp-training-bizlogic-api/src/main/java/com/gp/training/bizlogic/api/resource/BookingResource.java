package com.gp.training.bizlogic.api.resource;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.gp.training.bizlogic.api.model.BookingDTO;
import com.gp.training.bizlogic.api.params.BookingCreateParams;

@Path("/booking")
public interface BookingResource {
	
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public BookingDTO create(BookingCreateParams params);
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public List<BookingDTO> getAllBookings();
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("{cityId}")
	public BookingDTO getById(@PathParam("cityId") Long id);
	
	
}

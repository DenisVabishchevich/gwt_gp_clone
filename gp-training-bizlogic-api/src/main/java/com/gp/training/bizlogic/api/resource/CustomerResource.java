package com.gp.training.bizlogic.api.resource;

import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.gp.training.bizlogic.api.model.CustomerDTO;
import com.gp.training.bizlogic.api.params.CustomerCreateParams;

@Path("/customer")
public interface CustomerResource {

	@POST
	@Produces(MediaType.APPLICATION_JSON)
	public CustomerDTO create(CustomerCreateParams params);

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public List<CustomerDTO> getAllBookings();

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("{customerId}")
	public CustomerDTO getById(@PathParam("customerId") Long id);

}

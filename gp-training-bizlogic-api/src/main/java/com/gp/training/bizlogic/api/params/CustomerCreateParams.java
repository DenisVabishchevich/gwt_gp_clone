package com.gp.training.bizlogic.api.params;

public class CustomerCreateParams {

	private long bookingId;
	private String name;
	private String surname;

	public static class Builder {
		private long bookingId;
		private String name;
		private String surname;

		public Builder offerId(long bookingId) {
			this.bookingId = bookingId;
			return this;
		}

		public Builder guestCount(String name) {
			this.name = name;
			return this;
		}

		public Builder startDate(String surname) {
			this.surname = surname;
			return this;
		}


		public CustomerCreateParams build() {
			CustomerCreateParams params = new CustomerCreateParams();
			params.bookingId = bookingId;
			params.name = name;
			params.surname = surname;
			return params;
		}
	}

	public long getBookingId() {
		return bookingId;
	}

	public void setBookingId(long bookingId) {
		this.bookingId = bookingId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	
}

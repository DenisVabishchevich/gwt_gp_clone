package com.gp.training.bizlogic.rest;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.gp.training.bizlogic.api.model.CustomerDTO;
import com.gp.training.bizlogic.api.params.CustomerCreateParams;
import com.gp.training.bizlogic.api.resource.CustomerResource;
import com.gp.training.bizlogic.dao.CustomerDao;
import com.gp.training.bizlogic.domain.Customer;

@Component
public class CustomerResourceImpl implements CustomerResource {

	@Autowired
	private CustomerDao customerDao;

	@Override
	public CustomerDTO create(CustomerCreateParams params) {
		Customer customer = customerDao.addCustomer(params);
		return convertFromCustomer2DTO(customer);
	}

	@Override
	public List<CustomerDTO> getAllBookings() {
		List<Customer> customers = customerDao.getAll();
		List<CustomerDTO> customerDTO = new ArrayList<>();
		for (Customer c : customers) {
			CustomerDTO dto = convertFromCustomer2DTO(c);
			customerDTO.add(dto);
		}
		return customerDTO;
	}

	@Override
	public CustomerDTO getById(Long id) {
		Customer customer = customerDao.get(id);
		return convertFromCustomer2DTO(customer);
	}

	private CustomerDTO convertFromCustomer2DTO(Customer customer) {
		CustomerDTO dto = new CustomerDTO();
		if (customer != null) {
			dto.setId(customer.getId());
			dto.setBookingId(customer.getBooking().getId());
			dto.setName(customer.getName());
			dto.setSurname(customer.getSurname());

		}
		return dto;
	}

}

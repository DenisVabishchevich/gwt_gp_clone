package com.gp.training.bizlogic.domain;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "country")
public class Country extends AbstractEntity {
	
	@Column(name ="code")
	private String code;
	@Column(name ="name")
	private String name;
	@OneToMany(fetch=FetchType.LAZY)
	@JoinColumn(name = "country_id")
	private List<City> cities;
	
	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}

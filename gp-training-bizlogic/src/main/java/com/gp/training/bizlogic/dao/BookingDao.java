package com.gp.training.bizlogic.dao;

import com.gp.training.bizlogic.api.params.BookingCreateParams;
import com.gp.training.bizlogic.domain.Booking;

public interface BookingDao extends GenericDao<Booking, Long>{
	
	public Booking create(BookingCreateParams params);
}

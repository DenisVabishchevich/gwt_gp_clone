package com.gp.training.bizlogic.rest;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.gp.training.bizlogic.api.model.BookingDTO;
import com.gp.training.bizlogic.api.params.BookingCreateParams;
import com.gp.training.bizlogic.api.resource.BookingResource;
import com.gp.training.bizlogic.dao.BookingDao;
import com.gp.training.bizlogic.domain.Booking;

@Component
public class BookingResourceImpl implements BookingResource {

	@Autowired
	private BookingDao bookingDao;

	@Override
	public BookingDTO create(BookingCreateParams params) {
		Booking booking = bookingDao.create(params);
		return convertFromBookingToBookingDTO(booking);
	}

	private BookingDTO convertFromBookingToBookingDTO(Booking booking) {
		BookingDTO dto = new BookingDTO();
		if (booking != null) {
			dto.setId(booking.getId());
			dto.setGuestCount(booking.getGuestCount());
			dto.setEndDate(booking.getEndDate());
			dto.setStartDate(booking.getStartDate());
			dto.setOfferId(booking.getOffer().getId());

		}
		return dto;
	}

	@Override
	public List<BookingDTO> getAllBookings() {
		List<Booking> bookings = bookingDao.getAll();
		List<BookingDTO> bookingsDTO = new ArrayList<>();
		for (Booking b : bookings) {
			BookingDTO dto = convertFromBookingToBookingDTO(b);
			bookingsDTO.add(dto);
		}
		return bookingsDTO;
	}

	@Override
	public BookingDTO getById(Long id) {
		Booking booking = bookingDao.get(id);
		return convertFromBookingToBookingDTO(booking);
	}

}

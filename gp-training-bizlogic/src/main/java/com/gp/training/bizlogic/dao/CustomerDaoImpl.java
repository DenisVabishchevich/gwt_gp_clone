package com.gp.training.bizlogic.dao;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.support.rowset.SqlRowSet;
import org.springframework.stereotype.Repository;

import com.gp.training.bizlogic.api.params.CustomerCreateParams;
import com.gp.training.bizlogic.domain.Booking;
import com.gp.training.bizlogic.domain.Customer;

@Repository
public class CustomerDaoImpl extends GenericDaoImpl<Customer, Long> implements CustomerDao {
	
	@Autowired
	private NamedParameterJdbcTemplate namedJDBCTemplate;

	public static final String CREATE_CUSTOMER = "insert into customer (name, surname, bookingId) values ( :name, :surname, :bookingId);";
	
	public CustomerDaoImpl() {
		super(Customer.class);
	}


	@Override
	public Customer addCustomer(CustomerCreateParams params) {

		Map<String, Object> paramMap = parseParams(params);
		SqlRowSet queryForRowSet = namedJDBCTemplate.queryForRowSet(CREATE_CUSTOMER, paramMap);

		long id = queryForRowSet.getLong("id");

		Customer customer = new Customer();
		customer.setId(id);
		customer.setName(params.getName());
		customer.setSurname(params.getSurname());
		

		Booking booking = new Booking();
		booking.setId(params.getBookingId());
		customer.setBooking(booking);

		return customer;
	}

	private Map<String, Object> parseParams(CustomerCreateParams params) {
		Map<String, Object> paramMap = new HashMap<>();

		String name = params.getName();
		String surname = params.getSurname();
		long bookingId = params.getBookingId();

		paramMap.put("name", name);
		paramMap.put("surname", surname);
		paramMap.put("bookingId", bookingId);

		return paramMap;
	}
}
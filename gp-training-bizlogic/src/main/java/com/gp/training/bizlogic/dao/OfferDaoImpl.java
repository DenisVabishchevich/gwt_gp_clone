package com.gp.training.bizlogic.dao;

import com.gp.training.bizlogic.domain.Offer;

public class OfferDaoImpl extends GenericDaoImpl<Offer, Long> implements OfferDao {

	protected OfferDaoImpl() {
		super(Offer.class);

	}

}

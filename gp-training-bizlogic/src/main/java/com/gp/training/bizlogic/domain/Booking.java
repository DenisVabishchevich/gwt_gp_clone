package com.gp.training.bizlogic.domain;

import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "booking")
public class Booking extends AbstractEntity {
	
	@Column(name="start_date", insertable=true, nullable=false, unique=true, updatable=true)
	private String startDate;
	
	@Column(name="end_date", insertable=true, nullable=false, unique=true, updatable=true)
	private String endDate;
	
	@Column(name="guest_count", insertable=true, nullable=false, unique=true, updatable=true)
	private int guestCount;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "offer_id")
	private Offer offer;

	@OneToMany(fetch = FetchType.EAGER)
	@JoinColumn(name = "booking_id")
	private Set<Customer> customer;

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	public int getGuestCount() {
		return guestCount;
	}

	public void setGuestCount(int guestCount) {
		this.guestCount = guestCount;
	}

	public Offer getOffer() {
		return offer;
	}

	public void setOffer(Offer offer) {
		this.offer = offer;
	}

	public Set<Customer> getCostomers() {
		return customer;
	}

	public void setCostomers(Set<Customer> costomers) {
		this.customer = costomers;
	}

	
}

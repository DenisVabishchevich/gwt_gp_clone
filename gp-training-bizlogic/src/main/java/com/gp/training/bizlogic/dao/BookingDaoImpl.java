package com.gp.training.bizlogic.dao;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.support.rowset.SqlRowSet;
import org.springframework.stereotype.Repository;

import com.gp.training.bizlogic.api.params.BookingCreateParams;
import com.gp.training.bizlogic.domain.Booking;
import com.gp.training.bizlogic.domain.Offer;

@Repository
public class BookingDaoImpl extends GenericDaoImpl<Booking, Long> implements BookingDao {

	public static final String CREATE_BOOKING = "insert into booking (end_date, start_date, guest_count, offer_id) values ( :endDate, :startDate, :guestCount, :offerId);";

	@Autowired
	private NamedParameterJdbcTemplate namedJDBCTemplate;

	public BookingDaoImpl() {
		super(Booking.class);
	}

	@Override
	public Booking create(BookingCreateParams params) {

		Map<String, Object> paramMap = parseParams(params);
		SqlRowSet queryForRowSet = namedJDBCTemplate.queryForRowSet(CREATE_BOOKING, paramMap);

		long id = queryForRowSet.getLong("id");

		Booking booking = new Booking();
		booking.setId(id);
		booking.setStartDate(params.getStartDate().toString());
		booking.setEndDate(params.getEndDate().toString());
		booking.setGuestCount(params.getGuestCount());

		Offer offer = new Offer();
		offer.setId(params.getOfferId());
		booking.setOffer(offer);

		return booking;
	}

	private Map<String, Object> parseParams(BookingCreateParams params) {
		Map<String, Object> paramMap = new HashMap<>();

		String endDate = params.getEndDate().toString();
		String startDate = params.getStartDate().toString();
		int guestCount = params.getGuestCount();
		long offerId = params.getOfferId();

		paramMap.put("endDate", endDate);
		paramMap.put("startDate", startDate);
		paramMap.put("guestCount", guestCount);
		paramMap.put("offerId", offerId);

		return paramMap;
	}

}

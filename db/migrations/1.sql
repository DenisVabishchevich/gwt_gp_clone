-- Создание таблицы в базе.  Таблица пока просто для примера
create table hotel_info(
	id int(11) primary key  auto_increment, 
	name varchar(64) not null, 
	code varchar(10) not null, 
	description text not null
	) engine=InnoDB default charset=utf8;
	
-- Несколько отелей чтобы проверить работоспособность
insert into hotel_info(name, code, description) values 
	("My Hotel", "MMD", "The best of the best"), 
	("Spa Hotel", "SSP", "The most fashionable place in the world");
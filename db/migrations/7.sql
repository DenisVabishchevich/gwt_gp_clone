-- Таблица букингов
create table booking (
	id int(11) primary key auto_increment,
	offer_id int(11) not null,
	start_date varchar(10) not null,
	end_date varchar(10) not null,
	guest_count varchar(10) not null,
	foreign key (offer_id) references offer(id)
) engine=InnoDB default charset=utf8;
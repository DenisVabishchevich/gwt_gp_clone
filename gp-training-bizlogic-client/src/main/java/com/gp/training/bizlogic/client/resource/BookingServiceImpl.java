package com.gp.training.bizlogic.client.resource;

import java.util.Date;

import org.jboss.resteasy.client.jaxrs.ResteasyWebTarget;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.gp.training.bizlogic.api.model.BookingDTO;
import com.gp.training.bizlogic.api.params.BookingCreateParams;
import com.gp.training.bizlogic.api.resource.BookingResource;
import com.gp.training.bizlogic.client.ClientSender;
import com.gp.training.bizlogic.utils.DateUtils;

@Component
public class BookingServiceImpl implements BookingService {
	@Autowired
	private ClientSender sender;

	@Override
	public BookingDTO createBooking(BookingCreateParams params) {
		ResteasyWebTarget webTarget = sender.getRestEasyTarget();
		BookingResource bookingProxy = webTarget.proxy(BookingResource.class);

		// BookingCreateParams params = parseParamsFromDTO(booking);
		BookingDTO recivedBooking = bookingProxy.create(params);
		webTarget.getResteasyClient().close();
		return recivedBooking;
	}

	@Override
	public BookingDTO getBooking(Long id) {
		ResteasyWebTarget webTarget = sender.getRestEasyTarget();
		BookingResource bookingProxy = webTarget.proxy(BookingResource.class);
		BookingDTO recivedBooking = bookingProxy.getById(id);
		webTarget.getResteasyClient().close();
		return recivedBooking;
	}

	private BookingCreateParams parseParamsFromDTO(BookingDTO dto) {
		Date startDate = DateUtils.convertISODateStringToDate(dto.getStartDate());
		Date endDate = DateUtils.convertISODateStringToDate(dto.getEndDate());
		BookingCreateParams build = new BookingCreateParams.Builder().startDate(startDate).endDate(endDate)
				.guestCount(dto.getGuestCount()).offerId(dto.getOfferId()).build();
		return build;
	}

}

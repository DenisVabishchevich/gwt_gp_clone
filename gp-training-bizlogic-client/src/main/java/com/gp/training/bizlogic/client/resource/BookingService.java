package com.gp.training.bizlogic.client.resource;

import com.gp.training.bizlogic.api.model.BookingDTO;
import com.gp.training.bizlogic.api.params.BookingCreateParams;

public interface BookingService {
	
	public BookingDTO createBooking(BookingCreateParams booking);
	
	public BookingDTO getBooking(Long id);
}

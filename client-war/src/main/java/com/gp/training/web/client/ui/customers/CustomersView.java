package com.gp.training.web.client.ui.customers;

import com.google.gwt.core.client.Callback;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.Widget;
import com.gp.training.web.client.app.AppContext;
import com.gp.training.web.client.app.PageToken;
import com.gp.training.web.client.params.PageParams;
import com.gp.training.web.client.ui.common.GenericBaseView;
import com.gp.training.web.client.ui.common.PageBaseView;
import com.gp.training.web.client.ui.customers.customer.CustomerItem;
import com.gp.training.web.client.ui.customers.customer.CustomerPresenter;
import com.gp.training.web.shared.model.BookingProxy;
import com.gp.training.web.shared.params.BookingCreateParamsClient;
import com.sksamuel.jqm4gwt.button.JQMButton;
import com.sksamuel.jqm4gwt.list.JQMList;
import com.sksamuel.jqm4gwt.list.JQMListItem;

public class CustomersView extends GenericBaseView<CustomerPresenter> implements PageBaseView {

	private PageParams pageParams;

	@UiField
	protected JQMButton nextBtn;
	@UiField
	protected JQMList guestList;

	private static CustomersViewUiBinder uiBinder = GWT.create(CustomersViewUiBinder.class);

	interface CustomersViewUiBinder extends UiBinder<Widget, CustomersView> {
	}

	public CustomersView() {

		initWidget(uiBinder.createAndBindUi(this));
		initActions();

	}

	private BookingCreateParamsClient converAndSend(PageParams pageParams) {
		BookingCreateParamsClient bookingParams = new BookingCreateParamsClient.Builder()
				.startDate(pageParams.getStartDate()).endDate(pageParams.getEndDate())
				.guestCount(pageParams.getGuestCount()).offerId(pageParams.getOfferId()).build();
		return bookingParams;
	}

	private void initActions() {
		nextBtn.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				BookingCreateParamsClient params = converAndSend(pageParams);
//				getPresenter().createBooking(params, new Callback<BookingProxy, Void>() {
//
//					@Override
//					public void onSuccess(BookingProxy result) {
//						// TODO Auto-generated method stub
//
//					}
//
//					@Override
//					public void onFailure(Void reason) {
//					Window.alert("ERROR");
//
//					}
//				});
				getPresenter().getBooking(1L, new Callback<BookingProxy, Void>() {

					@Override
					public void onSuccess(BookingProxy result) {
						// TODO Auto-generated method stub

					}

					@Override
					public void onFailure(Void reason) {
					Window.alert("ERROR");

					}
				});

				PageParams newParams = new PageParams();
				AppContext.navigationService.next(PageToken.BOOKING, newParams);
			}
		});
	}

	@Override
	public void loadPageParams(PageParams params) {
		pageParams = params != null ? params : new PageParams();
		recreateGuestsList();
	}

	private void recreateGuestsList() {
		guestList.clear();

		int guestCount = pageParams.getGuestCount();

		for (int i = 0; i < guestCount; i++) {

			JQMListItem listItem = new JQMListItem();
			listItem.setControlGroup(true, false);

			CustomerItem customer = new CustomerItem();
			listItem.addWidget(customer);

			guestList.appendItem(listItem);
		}

		guestList.recreate();
		guestList.refresh();
	}
}

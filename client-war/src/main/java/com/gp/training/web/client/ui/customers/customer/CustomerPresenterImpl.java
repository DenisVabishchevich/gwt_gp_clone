package com.gp.training.web.client.ui.customers.customer;

import org.jboss.errai.common.client.api.RemoteCallback;
import org.jboss.errai.enterprise.client.jaxrs.api.RestClient;

import com.google.gwt.core.client.Callback;
import com.gp.training.bizlogic.api.params.BookingCreateParams;
import com.gp.training.web.shared.model.BookingProxy;
import com.gp.training.web.shared.params.BookingCreateParamsClient;
import com.gp.training.web.shared.service.BookingResource;

public class CustomerPresenterImpl implements CustomerPresenter {

	@Override
	public void createBooking(BookingCreateParamsClient params, Callback<BookingProxy, Void> callback) {
		RestClient.create(BookingResource.class, new RemoteCallback<BookingProxy>() {
			@Override
			public void callback(BookingProxy response) {
				if (callback != null)
					callback.onSuccess(response);
			}
		}).create(convertedParams(params));

	}

	@Override
	public void getBooking(Long id, Callback<BookingProxy, Void> callback) {
		RestClient.create(BookingResource.class, new RemoteCallback<BookingProxy>() {
			@Override
			public void callback(BookingProxy response) {
				if (callback != null)
					callback.onSuccess(response);
			}
		}).getById(id);
		
	}
	
	private BookingCreateParams convertedParams(BookingCreateParamsClient params){
		return new BookingCreateParams.Builder()
				.startDate(params.getStartDate())
				.endDate(params.getEndDate())
				.guestCount(params.getGuestCount())
				.offerId(params.getOfferId()).build();
				
	}

}

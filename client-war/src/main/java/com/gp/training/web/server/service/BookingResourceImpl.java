package com.gp.training.web.server.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.gp.training.bizlogic.api.model.BookingDTO;
import com.gp.training.bizlogic.api.params.BookingCreateParams;
import com.gp.training.bizlogic.client.resource.BookingService;
import com.gp.training.web.shared.model.BookingProxy;
import com.gp.training.web.shared.model.OfferProxy;
import com.gp.training.web.shared.service.BookingResource;

@Service
public class BookingResourceImpl implements BookingResource {
	@Autowired
	private BookingService bookingService;

	@Override
	public BookingProxy create(BookingCreateParams params) {
		BookingDTO createBooking = bookingService.createBooking(params);
		BookingProxy proxyBooking = new BookingProxy();
		proxyBooking.setStartDate(createBooking.getStartDate());
		proxyBooking.setEndDate(createBooking.getEndDate());
		proxyBooking.setGuestCount(createBooking.getGuestCount());

		OfferProxy offerProxy = new OfferProxy();
		offerProxy.setOfferId(Integer.parseInt(Long.valueOf(createBooking.getOfferId()).toString()));
		proxyBooking.setOffer(offerProxy);

		return proxyBooking;
	}

	

	@Override
	public BookingProxy getById(Long id) {
		BookingDTO createBooking = bookingService.getBooking(id);
		BookingProxy proxyBooking = new BookingProxy();
		proxyBooking.setStartDate(createBooking.getStartDate());
		proxyBooking.setEndDate(createBooking.getEndDate());
		proxyBooking.setGuestCount(createBooking.getGuestCount());

		OfferProxy offerProxy = new OfferProxy();
		offerProxy.setOfferId(Integer.parseInt(Long.valueOf(createBooking.getOfferId()).toString()));
		proxyBooking.setOffer(offerProxy);

		return proxyBooking;
	}

}

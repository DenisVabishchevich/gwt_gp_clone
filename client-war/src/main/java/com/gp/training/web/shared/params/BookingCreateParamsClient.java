package com.gp.training.web.shared.params;

import java.util.Date;

import org.jboss.errai.common.client.api.annotations.MapsTo;
import org.jboss.errai.common.client.api.annotations.NonPortable;
import org.jboss.errai.common.client.api.annotations.Portable;

@Portable
public class BookingCreateParamsClient {

	private Date startDate;
	private Date endDate;
	private int guestCount;
	private long offerId;

	public BookingCreateParamsClient(@MapsTo("startDate") Date startDate, @MapsTo("endDate") Date endDate,
			@MapsTo("guestCount") int guestCount, @MapsTo("offerId") long offerId) {

		this.startDate = startDate;
		this.endDate = endDate;
		this.guestCount = guestCount;
		this.offerId = offerId;
	}

	@NonPortable
	public static class Builder {
		private long offerId;
		private int guestCount;
		private Date startDate;
		private Date endDate;

		public Builder offerId(long offerId) {
			this.offerId = offerId;
			return this;
		}

		public Builder guestCount(int guestCount) {
			this.guestCount = guestCount;
			return this;
		}

		public Builder startDate(Date startDate) {
			this.startDate = startDate;
			return this;
		}

		public Builder endDate(Date endDate) {
			this.endDate = endDate;
			return this;
		}

		public BookingCreateParamsClient build() {
			BookingCreateParamsClient params = new BookingCreateParamsClient(startDate, endDate, guestCount, offerId);
			return params;
		}
	}

	public long getOfferId() {
		return offerId;
	}

	public int getGuestCount() {
		return guestCount;
	}

	public Date getStartDate() {
		return startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

}

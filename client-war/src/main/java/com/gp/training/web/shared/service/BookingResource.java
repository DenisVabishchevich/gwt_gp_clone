package com.gp.training.web.shared.service;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.gp.training.bizlogic.api.params.BookingCreateParams;
import com.gp.training.web.shared.model.BookingProxy;

@Path("/booking")
public interface BookingResource {

	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public BookingProxy create(BookingCreateParams params);

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("{cityId}")
	public BookingProxy getById(@PathParam("cityId") Long id);
}

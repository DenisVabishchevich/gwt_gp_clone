package com.gp.training.web.shared.model;

import org.jboss.errai.common.client.api.annotations.Portable;

@Portable
public class CustomerProxy {

	private String name;

	private String surname;

	private BookingProxy bookings; 
	
	

	public CustomerProxy() {
		}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public BookingProxy getBookings() {
		return bookings;
	}

	public void setBookings(BookingProxy bookings) {
		this.bookings = bookings;
	}

}

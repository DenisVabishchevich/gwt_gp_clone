package com.gp.training.web.client.ui.customers.customer;

import com.google.gwt.core.client.Callback;
import com.gp.training.web.client.ui.common.Presenter;
import com.gp.training.web.shared.model.BookingProxy;
import com.gp.training.web.shared.params.BookingCreateParamsClient;

public interface CustomerPresenter extends Presenter{
	
	public void createBooking(BookingCreateParamsClient params, Callback<BookingProxy, Void> callback);

	public void getBooking(Long id, Callback<BookingProxy, Void> callback);
}

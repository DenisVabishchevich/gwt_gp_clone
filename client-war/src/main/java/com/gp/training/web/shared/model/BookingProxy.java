package com.gp.training.web.shared.model;

import org.jboss.errai.common.client.api.annotations.MapsTo;
import org.jboss.errai.common.client.api.annotations.Portable;

@Portable
public class BookingProxy {

	private String startDate;

	private String endDate;

	private int guestCount;

	private OfferProxy offer;

	public BookingProxy() {
	}

//	public BookingProxy(@MapsTo("startDate") String startDate, @MapsTo("endDate") String endDate,
//			@MapsTo("guestCount") int guestCount, @MapsTo("offer") OfferProxy offer) {
//		super();
//		this.startDate = startDate;
//		this.endDate = endDate;
//		this.guestCount = guestCount;
//		this.offer = offer;
//	}

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	public int getGuestCount() {
		return guestCount;
	}

	public void setGuestCount(int guestCount) {
		this.guestCount = guestCount;
	}

	public OfferProxy getOffer() {
		return offer;
	}

	public void setOffer(OfferProxy offer) {
		this.offer = offer;
	}

}
